package com;

public class App {

    static App app=new App();

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        App app = test();
        System.out.println(app == App.app);
    }

    static App test() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        StackTraceElement stack = Thread.currentThread().getStackTrace()[2];
        System.out.println(stack.getClassName());
        System.out.println(stack.getFileName());
        System.out.println(stack.getLineNumber());
        System.out.println(stack.getMethodName());
        System.out.println(stack);
        Class c = Class.forName(stack.getClassName());
        return (App) c.newInstance();
    }
}
