package com.study;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

/**
 * RSA 类是用来做
 *
 * @author FangXin pass ‘GERJ’ computer edit code
 * @version 1.0
 * @date 2021/1/4 17:36
 * @project_name study
 * @class_name RSA
 * @package_name com.study
 */
public class RSATest {

    public static void main(String[] args) {

        final String CHAT_SET = "UTF-8";

        //原密码
        String data = "RSA 加解密测试!";
        System.out.println("==============================原密码：" + data);

        KeyPair pair1 = getKeyPair();
        byte[] publicKey = pair1.getPublic().getEncoded();
        byte[] privateKey = pair1.getPrivate().getEncoded();
        byte[] encrypt = null;
        byte[] decrypt = null;

        {
            System.out.println("================================私钥加密公钥解密===================================");
            encrypt = encryptByPrivateKey(data.getBytes(), privateKey);
            System.out.println("使用私钥加密==================");
            System.out.println("秘文：" + new String(encrypt));
            decrypt = decryptByPublicKey(encrypt, publicKey);
            System.out.println("使用公钥解密==================");
            System.out.println("明文：" + new String(decrypt));
        }
        {
            System.out.println("================================公钥加密私钥解密===================================");
            encrypt = encryptByPublicKey(data.getBytes(), publicKey);
            System.out.println("使用公钥加密==================");
            System.out.println("秘文：" + new String(encrypt));
            decrypt = decryptByPrivateKey(encrypt, privateKey);
            System.out.println("使用私钥解密==================");
            System.out.println("明文：" + new String(decrypt));
        }

    }

    static String algorithm = "rsa";

    static KeyFactory factory;

    static Cipher cipher;

    static {
        try {
            cipher = Cipher.getInstance(algorithm);
            factory = KeyFactory.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public static KeyPair getKeyPair() {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
            kpg.initialize(512);
            return kpg.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PublicKey getPublicKey(byte[] key) {
        try {
            return factory.generatePublic(new X509EncodedKeySpec(key));
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PrivateKey getPrivateKey(byte[] key) {
        try {
            return factory.generatePrivate(new PKCS8EncodedKeySpec(key));
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] encryptByPublicKey(byte[] data, byte[] publicKey) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
            return doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] encryptByPrivateKey(byte[] data, byte[] privateKey) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, getPrivateKey(privateKey));
            return doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] doFinal(byte[] data) {
        try {
            return cipher.doFinal(data);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] decryptByPublicKey(byte[] data, byte[] publicKey) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, getPublicKey(publicKey));
            return doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] decryptByPrivateKey(byte[] data, byte[] privateKey) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKey));
            return doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

}
