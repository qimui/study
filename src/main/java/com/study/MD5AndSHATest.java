package com.study;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Locale;

/**
 * MD5Test 类是用来做
 *
 * @author FangXin pass ‘GERJ’ computer edit code
 * @version 1.0
 * @date 2021/1/4 14:25
 * @project_name study
 * @class_name MD5Test
 * @package_name com.study
 */
public class MD5AndSHATest {

    public static void main(String[] args) throws Exception {
        String str = MD5("654321\"", "md5");
        System.out.println("md5加密后：" + str);
        str = MD5("654321\"", "sha-256");
        System.out.println("sha加密后：" + str);
    }

    /**
     * 目前所知性能最优解
     *
     * @param src
     * @return
     */
    public static String MD5(String src, String type) {
        //md5的长度是32位
        int len = 32;
        if ("sha".equalsIgnoreCase(type)) {
            //sha的长度是40位
            len = 40;
        } else if ("sha-256".equalsIgnoreCase(type)) {
            //sha256的长度是64位
            len = 64;
        }
        final char[] result = new char[len];
        try {
            byte[] results = MessageDigest.getInstance(type).digest(src.getBytes());
            for (int i = 0, j = 0; i < results.length; i++) {
                int num = results[i] & 0x0000ff;
                if (num < 16) {
                    result[j++] = '0';
                }
                String str = Integer.toHexString(num);
                for (int k = 0; k < str.length(); k++) {
                    result[j++] = str.charAt(k);
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return new String(result);
    }


}
