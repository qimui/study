package com.study;


import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * DESTEST 类是用来做
 *
 * @author FangXin pass ‘GERJ’ computer edit code
 * @version 1.0
 * @date 2021/1/4 16:47
 * @project_name study
 * @class_name DESTEST
 * @package_name com.study
 */
public class DESAndAESTest {

    public static void main(String[] args) {
        type = "AES";
        SecretKey key = getSecretKey();
        String str = "FangXin pass ‘GERJ’ computer edit code";
        System.out.println("原文：" + str);
        byte[] en = encrypt(str.getBytes(), key.getEncoded());
        System.out.println("加密后：" + new String(en));
        en = encrypt(en, key.getEncoded());
        en = decrypt(en, key.getEncoded());
        System.out.println("解密后：" + new String(en));
    }

    private static String type = "des";

    public static SecretKey getSecretKey() {
        try {
            KeyGenerator generator = KeyGenerator.getInstance(type);
            return generator.generateKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Cipher getCipher() {
        try {
            return Cipher.getInstance(type);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] encrypt(byte[] data, SecretKey key) {
        Cipher cipher = getCipher();
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] decrypt(byte[] data, SecretKey key) {
        Cipher cipher = getCipher();
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            return cipher.doFinal(data);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] encrypt(byte[] data, byte[] key) {
        return encrypt(data, new SecretKeySpec(key, type));
    }

    public static byte[] decrypt(byte[] data, byte[] key) {
        return decrypt(data, new SecretKeySpec(key, type));
    }


}
