package com.study;

import java.util.Base64;

/**
 * Base64Test 类是用来做
 *
 * @author FangXin pass ‘GERJ’ computer edit code
 * @version 1.0
 * @date 2021/1/4 14:20
 * @project_name study
 * @class_name Base64Test
 * @package_name com.study
 */
public class Base64Test {

    public static void main(String[] args) {
        String str = "这是加密前";
        System.out.println("base64加密，加密前的字符串是：" + str);
        String newStr = new String(Base64.getEncoder().encode(str.getBytes()));
        System.out.println("encode()方法加密后的字符串是：" + newStr);
        newStr = Base64.getEncoder().encodeToString(str.getBytes());
        System.out.println("encodeToString()方法加密后的字符串是：" + newStr);
    }
}
